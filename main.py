# Third party
import numpy as np
from scipy import interpolate
from detect_peaks import detect_peaks
import matplotlib.pyplot as plt
import biosig

#global path
### Prueba en Matlab-Biosig
# Data load
#dataset = biosig.data(path)
dataset = biosig.data('Dataset1.hl7')  ### TO BE GUI DEFINED ###

# Counting number of channels and defining length.
length,channels = dataset.shape

# Taking the data for every channel
#Bad practice:
for i in range(1,channels):
    globals()['ch%s' % i]= dataset[0:length,i]
#Other bad practice:
#dataset_ch1 = dataset[:,0]
#dataset_ch2 = dataset[0:length,1]
#dataset_ch3 = dataset[0:len(dataset),2]


#-----------------------------------------------------Sample for testing
dataset = dataset[10000:15000,0]
#-----------------------------------------------------------------------


fs = 250  ##### TO BE GUI DEFINED ###
N = length/fs # seconds
t = np.linspace(0, N, len(dataset))

fig = plt.figure()  # an empty figure with no axes
tmin = 000  ### TESTING
tmax = 5000  ### TESTING

plt.plot(t[tmin:tmax], dataset[tmin:tmax], label='Channel 1')
plt.ylim((min(dataset), max(dataset)))
plt.show()


#-----------------------------------------------------------------------
# Re-sampling at 1000 samples/sec
fs2=1000   ### TO BE GUI DEFINED ###
t1000 = np.linspace(0, N, len(dataset)*int((fs2/fs)))
tck = interpolate.splrep(t, dataset, s=0)
dataset_1000 = interpolate.splev(t1000, tck, der=0)


fig = plt.figure()  # an empty figure with no axes
plt.plot(t1000[int(tmin*(fs2/fs)):int(tmax*(fs2/fs))], dataset_1000[int(tmin*(fs2/fs)):int(tmax*(fs2/fs))], label='Channel 1')
plt.ylim((min(dataset_1000), max(dataset_1000)))
plt.show()



#-----------------------------------------------------------------------
# Find R peaks
M = max(dataset)
m = -min(dataset)
thr = 0.0 # seconds ### TO BE GUI DEFINED ###
indexes_M = detect_peaks(dataset, mph=M/2, mpd=fs/2, threshold=thr*fs).tolist()
indexes_M[:] = [(x / fs) for x in indexes_M] # convert to seconds

indexes_m = detect_peaks(-dataset, mph=m/2, mpd=fs/2, threshold=thr*fs).tolist()
indexes_m[:] = [(x / fs) for x in indexes_m] # convert to seconds


# Time difference between peaks
if len(indexes_M)>len(indexes_m):   # use the max amount of peaks
    peaks_diff = [x - y for x, y in zip(indexes_M[1:], indexes_M[:-1])]
else:
    peaks_diff = [x - y for x, y in zip(indexes_m[1:], indexes_m[:-1])]
peaks_diff[:] = [60/x for x in peaks_diff] # convert to bpm


peaks_diff_tarray = np.linspace(1,(max(indexes_m)),len(indexes_m)).tolist() #second of the indexes
plt.plot(peaks_diff_tarray[:-1],peaks_diff)



#-----------------------------------------------------------------------
# Define a range (multiple times looping) and get the max, min and mean of every range.
# The values are stored at rangesmax, rangesmin, rangesmean
# Target: Candlestick graph
rangesmax = []
rangesmin = []
rangesmean = []
openrangesmean = []
closerangesmean = []
for i in range(1,5): #TO BE DEFINED ###
    globals()['dataset%s' % i]= dataset[(i-1)*100:(i*150)] ### TO BE DEFINED ###
    rangevalues = ['dataset%s' % i]
    rangesmax.append(np.max(eval(rangevalues[0])))
    rangesmin.append(np.min(eval(rangevalues[0])))
    rangesmean.append(np.mean(eval(rangevalues[0])))
    
# For opening data and closing data, a range of 1/10 of the range is taken at the beginnig and at the end
# As a result, the data of the 1/10 and 9/10s is taken for open/close values
    openrangesvalues = eval(rangevalues[0])[0:int(len(eval(rangevalues[0]))/10)]
    closerangesvalues = eval(rangevalues[0])[int(len(eval(rangevalues[0])))-int(len(eval(rangevalues[0]))/10):]
    openrangesmean.append(np.mean(openrangesvalues))
    closerangesmean.append(np.mean(closerangesvalues))
    

    
#-----------------------------------------------------------------------

    

#fs = hd.SampleRate;  # fs = 250
#N = length(ecg1);
##t=(1:N)'/fs;
#plot(t, -ecg1(:,1))  # Derivación 1 (ver hd.Label)
#plot(t, -ecg1(:,3))  # Derivación 3, parece ser V1 (ver hd.Label)
# Encontar los picos de la onda R con función findpeaks de matlab
# Busacamos picos a partir del máximo del ECG /2
#m=max(-ecg1(:,1))
#[pic,pos] = findpeaks(-ecg1(:,1), 'MinPeakHeight',m/2);
#plot(t, -ecg1(:,1),pos/fs,pic,'or')
# Vamos remuestrear a 1000 samp/sec con splines
#fs2 =1000;
#t1000 = (1/fs2:1/fs2:t(end));
#ecg1000 = spline(t',ecg1',t1000);
#[pic2,pos2] = findpeaks(-ecg1000(1,:),'MINPEAKHEIGHT',m/2);
#plot(t, -ecg1(:,1),t1000,-ecg1000(1,:),pos/fs,pic,'or',pos2/f2,pic2,'+g')
# Observa un pico de R de un latido y verás la mejora al interpolar
#####
