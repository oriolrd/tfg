import tkinter as tk
from tkinter import font  as tkfont
from tkinter import filedialog


#global path
path = []

class SampleApp(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        self.title_font = tkfont.Font(family='Helvetica', size=18, weight="bold", slant="italic")

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (StartPage, PageOne, PageTwo):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("StartPage")

    def show_frame(self, page_name):
        '''Show a frame for the given page name'''
        frame = self.frames[page_name]
        frame.tkraise()
        



class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Welcome to the ECG analysis tool", font=controller.title_font)
        label.pack(side="top", fill="x", padx=100, pady=50)

        button = tk.Button(self, text="Next",
                            command=lambda: controller.show_frame("PageOne"))
        button.pack()


class PageOne(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="Select a File", font=controller.title_font)
        label.pack(side="top", fill="x", padx=100, pady=50)
        
        selectButton = tk.Button(self, text="Select File",
                            command=self.selectFile)
        button1 = tk.Button(self, text="Back",
                           command=lambda: controller.show_frame("StartPage"))
        button2 = tk.Button(self, text="Next",
                           command=lambda: controller.show_frame("PageTwo"))
        selectButton.pack()
        button1.pack()
        button2.pack()
        
        

    def selectFile(self):
        global path
#        tk.filename =  tk.filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("all files","*.*"),("jpeg files","*.jpg")))
        path =  tk.filedialog.askopenfilename(initialdir = "/home/oriolrd/AnacondaProjects/TFG/",title = "Select file",filetypes = (("all files","*.*"),("jpeg files","*.jpg")))


class PageTwo(tk.Frame):
    
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        label = tk.Label(self, text="This is page 2", font=controller.title_font)
        label.pack(side="top", fill="x", pady=10)
        button = tk.Button(self, text="Go to the start page",
                           command=lambda: controller.show_frame("StartPage"))
        calcButton = tk.Button(self, text="Run",
                           command=self.calc)
        button.pack()
        calcButton.pack()
        
        
        
    def calc(self):
        import test01_pedro






if __name__ == "__main__":
    app = SampleApp()
    app.mainloop()