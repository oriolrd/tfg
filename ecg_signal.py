#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 11:01:58 2017
Module used for processing the electrocardiographic signals.

@author: oriolrd
"""

import numpy as np
from scipy import interpolate
import biosig
import detect_peaks

def signal(dataset=None, fs=1000):
    # Check if the signal exists and is valid
    if dataset is None:
        raise TypeError("The given signal is not valid.")
    dataset = np.array(dataset)
    
    # Set the time
    t = len(dataset)/fs
    T = np.linspace(0, t, len(dataset))
    
        
#    if fs < 1000:
    dataset, fs = interp(dataset, fs)
        
#    return dataset



def interp(dataset,fs):
    fs2=1000
    N = len(dataset)/fs
    t = np.linspace(0, N, len(dataset))
    t1000 = np.linspace(0, N, len(dataset)*int((fs2/fs)))
    tck = interpolate.splrep(t, dataset, s=0)
    dataset_1000 = interpolate.splev(t1000, tck, der=0)
    
    return dataset_1000, fs2