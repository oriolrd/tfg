#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 24 10:57:16 2018

@author: oriolrd
"""

# Third party
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import biosig
import pywt
from pywt import wavedec
from scipy import stats
import os
import sys
from time import time
from datetime import datetime
import csv

### Logging -------------------------
import logging
LOG_FILENAME = 'wavelets_test.log'
logging.basicConfig(filename=LOG_FILENAME)


#Files to process may be stored at '/data' subfolder
datapath = os.getcwd()+'/data/'

plt.close('all')
figureindex = 1
unit = ''

DATA = []

for filename in os.listdir(datapath):
    if filename.endswith(".hl7"):
        # Data load
        #dataset = biosig.data(path)
        #filename = 'Dataset1.hl7'
        dataset_all = biosig.data(datapath+filename) 
        header = biosig.header(datapath+filename)
        unit = header[header.find('PhysicalUnit')+17:header.find('PhysicalUnit')+header[header.find('PhysicalUnit'):].find(',')-1]
        if unit == 'uV':
            dataset_all = dataset_all/1000
        
        # Counting number of channels and defining length.
        length,channels = dataset_all.shape
        
        # Taking the data for every channel
        channeliterations = np.arange(0,channels)
        #channeliterations = np.arange(0,1)
        channelBPM = []
        for channeliteration in np.nditer(channeliterations, op_flags=['readwrite']):
            
                fs = 250
                #fs = int(float(header[header.find('Samplingrate')+16:header.find('Samplingrate')+header[header.find('Samplingrate'):].find(',')]))
                timeframe = 20
#            try:
                #-----------------------------------------------------Sample for testing
                #firstpos = 200340
#                firstpos = int((2000)*60*fs/20)
                firstpos = 1800*fs # After 30'
                
                maxiterations = int(len(dataset_all[:,channeliteration])/(timeframe*fs))
                iterations = maxiterations
                iterations = 1
                iterations = 540 #3h a timeframe=20s
                
                if iterations > 1:
                    plotting = 0
                else:
                    plotting = 1
                
                iterationnumber = np.arange(iterations)
                meanBPM = np.arange(iterations)
                clipped = np.zeros(iterations)
                
                for iteration in np.nditer(iterationnumber, op_flags=['readwrite']):
                    
                    N = length/fs # seconds
                    
                    iteratingpos = firstpos + (fs*timeframe) * iteration
                    dataset_range = dataset_all[iteratingpos:iteratingpos+(fs*timeframe),channeliteration] 
                    
                    #Invert signal
                    dataset_range = -dataset_range
                    
                    t = np.linspace(0, N, len(dataset_range))

                    #fs2=1000   
                    #t1000 = np.linspace(0, N, len(dataset_range)*int((fs2/fs)))
                    #tck = interpolate.splrep(t, dataset_range, s=0)
                    #dataset = interpolate.splev(t1000, tck, der=0)
                    dataset = dataset_range
                    #-----------------------------------------------------------------------
                    
                    
                    lvl = 3
                    coeffs = wavedec(dataset, 'sym4', level=lvl)
                    cA3, cD3, cD2, cD1 = coeffs
                    
                    
                    
                    #-----------------------------------------------------------------------
                    #Detect if the dataset is clipped
                    #if (max(dataset) == 4956.3500000000004) & (min(dataset) == -5017.6000000000004):
                    if (max(dataset) >= 4500) & (min(dataset) <= -4500):
                        clipped[iteration] = 1
                    
                    #-----------------------------------------------------------------------
                    
#                    # THRESHOLDING
#                    #Universal Threshold 
#                    uTh = np.sqrt(2*np.log(len(dataset)))*np.median(abs(cD1))/0.6745
#                    cA3_uTh = pywt.threshold(cA3,uTh,mode='soft')
#                    
#                    
#                    
#                    # Custom CUSUM
#                    #Five values are taken (position +-2). Peaks may be hardened.
#                    cusum = np.arange(len(cD2)-2)
#                    positions = cusum[:-2]+2
#                    for i in np.nditer(positions, op_flags=['readwrite']):
#                        cusum[i] = abs(cD2[i-2])+abs(cD2[i-1])+abs(cD2[i])+abs(cD2[i+1])+abs(cD2[i+2])
#                    
#                    # Detection of CUSUM peaks.
#                    # Threshold set to 1000. If exceeded, find max of [value:value+25]
#                    peakval = []
#                    peakpos = []
#                    peakdiff = [0]
#                    counter = 0
#                    avgpos = 0
#                    SDpos = 0
#                    threshold = max(cusum)/np.pi
#                    while (counter < len(cusum)):
#                        # If the value is over threshold, get range and get max value
#                        if cusum[counter] >= threshold:            
#                            # Set the frame size
#                            prevmeancounts = meanBPM[iteration-1]*(fs/2**lvl)/60
#                            frameval = int(prevmeancounts/2)
#                            if (iteration < 5) & (len(meanBPM) == 0) & (iteration > 0) | (frameval < 10):
#                                frameval = int(fs/10)
#                            try:
#                                frame = cusum[counter:counter+int(frameval)] 
#                            except:
#                                frame = cusum[counter:counter+(len(cusum)-counter)]        
#                            maxi = frame.argmax() 
#                            maxv = frame[maxi] 
#                            peakval = np.append(peakval, maxv)
#                            peakpos = np.append(peakpos, counter)
#        
#                            if (len(peakpos) > 1):
#                                if ((avgpos > 0) & (SDpos > 0)):
#                                    #Repeating the process several times if several errors. 
#                                    #Changed 'if' loop to 'while'
#                                    #else executed when while is false
#                                    while ((counter-peakpos[-2]) > (2*avgpos-SDpos)):
#                                        peakdiff = np.append(peakdiff, (counter-peakpos[-2])/2)
#                                        peakpos[-1] = peakpos[-1]-peakdiff[-1]
#                                        peakval[-1] = cusum[int(peakpos[-1])]
#                                        peakdiff = np.append(peakdiff, counter-peakpos[-2])
#                                        peakpos = np.append(peakpos, counter)
#                                        peakval = np.append(peakval, cusum[int(peakpos[-1])])
#                                        logging.error('File '+str(filename)+', channel '+str(channeliteration)+', iteration '+str(iteration)+'. Possible arrhythmia detected.')
#
#                                    else:
#                                        peakdiff = np.append(peakdiff,counter-peakpos[-2])
#                                else:
#                                    peakdiff = np.append(peakdiff, counter-peakpos[-2])
#                                    
#                            if (len(peakdiff[1:]) > 1):
#                                #First value [0] is set manually (0)
#                                #Second value [1] is based on the first reference and therefore may not be valid
#                                avgpos = np.average(peakdiff[2:])
#                                SDpos = np.std(peakdiff[2:])
#                                
#                            counter = counter + (frameval-1)
#                        counter = counter + 1
#                    
                    
                    ### DetecRC start
                    senyal = dataset_range
                    derSenyal = np.diff(senyal)
                    numMostres = len(derSenyal)
                    offset = 50
                    llindar = .75 * max(derSenyal[1:2500])
                    comptador = 0
                    mostra = 0
                    marques = []
                    valors = []
                    
                    # BUCLE PRINCIPAL DE CERCA
                    continuar = True
                    while continuar:
                        if derSenyal[mostra] > llindar:
                            buscar = True
                            while buscar:
                                if derSenyal[mostra] < 0:
                                    comptador = comptador + 1
                                    marques = np.append(marques,mostra)
                                    valors = np.append(valors,senyal[mostra])
                                    buscar = False
                                    llindar = np.mean([llindar, .4 * max(derSenyal[mostra:min([mostra + (2500), numMostres])])])
                                    #print(mostra)
                                    if mostra + offset >= numMostres-1:
                                        continuar = False
                                        break
                                    else:
                                        mostra = mostra + offset
                                        
                                else:
                                    if mostra + offset >= numMostres-1:
                                        continuar = False
                                        break
                                    else:
                                        mostra = mostra + 1
                                        
                        else:
                            if mostra + offset >= numMostres-1:
                                continuar = False
                                break
                            else:
                                mostra = mostra + 1
                    
                    peakpos = marques 
                    peakval = valors
                    ### DetecRC end 
                                                    
                    
                    # BPM
                    #lvl 3 = 2**3
                    # Difference * freq * conversion s-min
                    BPM = np.arange(len(peakpos)-1)
                    positions = BPM[:]
                    if (len(BPM) > 0):
                        for i in np.nditer(positions, op_flags=['readwrite']):
                            BPM[i] = (peakpos[i+1]-peakpos[i])/(fs/2**lvl)*60
                        
                        #Set 230 as the maximum value of BPM that can appear
                        if (np.mean(BPM) <= 230): 
                            reductionnum = 0
                            while (meanBPM[iteration] == iteration) & (reductionnum == 0):
                                if len(BPM) <= 6-(2*reductionnum):
                                    reductionnum = reductionnum + 1
                                elif reductionnum == 3:
                                    meanBPM[iteration] = meanBPM[iteration-1]
                                else:
                                    #Excluding the three most extreme values (3 for low, 3 for high)
                                    if (np.mean(np.sort(BPM)[3:len(BPM)-3]) <= 230):
                                        meanBPM[iteration] = np.mean(np.sort(BPM)[3:len(BPM)-3])
                                        reductionnum = reductionnum + 1
                                    else:
                                        meanBPM[iteration] = meanBPM[iteration-1]
                        else:
                            if (iteration > 0) & (meanBPM[iteration-1]<300):
                                meanBPM[iteration] = meanBPM[iteration-1]
                            else:
                                meanBPM[iteration] = 0
                    else:
                        meanBPM[iteration] = 0
                    

                
                
                    #Peaks at the original signal
                    peakvaloriginal = []
                    peakposoriginal = peakpos#*2**(lvl-1)
                    peakvaloriginal = dataset[peakposoriginal.astype(int).tolist()]
                    
                    Tsval = []
                    Tspos = []
                    for i in np.arange(len(peakposoriginal)-1):
                        distance = peakposoriginal[i+1]-peakposoriginal[i]
                        initial = int(peakposoriginal[i] + int(distance/6))
                        final = int(peakposoriginal[i] + int(distance*0.6))
                        Tsval.append(max(dataset[initial:final]))
                        Tspos.append(initial+dataset[initial:final].argmax())
                            
                        
                    Psval = []
                    Pspos = []
                    lessloops = 0
                    if len(peakposoriginal)==len(Tspos):
                        lessloops = 1
                    for i in np.arange(len(Tspos)-lessloops):
                        distance = peakposoriginal[i+1]-Tspos[i]
                        initial = int(Tspos[i] + int(distance/6))
                        final = int(peakposoriginal[i+1] - int(distance/6))
                        Psval.append(max(dataset[initial:final]))
                        Pspos.append(initial+dataset[initial:final].argmax())
                    
                    
                    
                    #------PLOTTING----------------------------------------------------------
                    if plotting == 1 :
                        plt.close('all')
                        
                        # Figure 1
                        #plt.figure(figureindex,figsize=(20,10))
                        #figureindex = figureindex + 1
                        
                        plt.xticks(np.arange(0, 20, step=2))

                        #plt.ylabel('Signal')
                        xs = np.linspace(0, (len(cD1)*2**(lvl-2)/fs), len(cD1))
                        
                        f, axarr = plt.subplots(5, 1)
                        
                        axarr[0].plot(dataset)
                        axarr[0].set_title('Original signal')
                        axarr[1].plot(cA3)
                        axarr[1].set_title('cA3')
                        axarr[2].plot(cD3)
                        axarr[2].set_title('cD3')
                        axarr[3].plot(cD2)
                        axarr[3].set_title('cD2')
                        axarr[4].plot(xs,cD1)
                        axarr[4].set_title('cD1')
                        plt.xlabel('Seconds')
                        plt.show()
#                        
#                        
#                        # Figure 2: Original signal vs cA3. Peaks hardened.
#                        plt.figure(figureindex,figsize=(20,10))
#                        figureindex = figureindex + 1
#                        tcA3 = np.linspace(0,N,len(cA3))
#                        plt.plot(tcA3,cA3, label='cA3')
#                        plt.plot(t,dataset, label='dataset')
#                        plt.legend(loc='best')
#                        
#                        
#                        # Figure 3: cA3 vs cA3 with universal threshold - nothing special displayed.
#                        plt.figure(figureindex,figsize=(20,10))
#                        figureindex = figureindex + 1
#                        tcA3_uTh = np.linspace(0,N,len(cA3_uTh))
#                        plt.plot(tcA3,cA3, label='cA3')
#                        plt.plot(tcA3_uTh,cA3_uTh, label='cA3_uTh')
#                        plt.legend(loc='best')
#                        
#                        
#                        # Figure 4: plot of the CUSUM
#                        plt.figure(figureindex,figsize=(20,10))
#                        figureindex = figureindex + 1
#                        plt.plot(cusum)
                        
                        
                        # Figure 5: plot of the CUSUM + positions detected (apparently peaks)
                        plt.figure(figureindex,figsize=(20,10))
                        figureindex = figureindex + 1
                        plt.xticks(np.arange(0, 20, step=2))
                        plt.xlabel('Seconds')
                        plt.ylabel('CUSUM')
                        xs = np.linspace(0, (len(cusum)*2**(lvl-1)/fs), len(cusum))
                        
                        plt.plot(xs, cusum)
                        plt.plot(peakpos*2**(lvl-1)/fs, peakval, 'rD')
                        #plt.plot(peakpos2, peakval2, 'rD')
                        
                        # Figure 6: plot of the original signal + positions detected (apparently peaks)
                        plt.figure(figureindex,figsize=(20,10))
                        figureindex = figureindex + 1
                        plt.xticks(np.arange(0, len(dataset)/fs, step=2))
                        plt.xlabel('Seconds')
                        plt.ylabel('mV')
                        xs = np.linspace(0, (len(dataset)/fs), len(dataset))
                        
                        plt.plot(xs, dataset)
                        Ts, = plt.plot(np.asarray(Tspos)/fs, Tsval, 'rD', label='Onda T')
                        Ps, = plt.plot(np.asarray(Pspos)/fs, Psval, 'mD', label='Onda P')
                        for xc in peakposoriginal/fs:
                            if xc == (peakposoriginal/fs)[0]:
                                plt.axvline(x=xc, color='g', linestyle='--', label='Onda R')
                            else:
                                plt.axvline(x=xc, color='g', linestyle='--')
                        #plt.legend(['Señal', 'Onda T', 'Onda P'])
                        plt.legend()

                        # DetecRC plot
                        plt.figure(figureindex,figsize=(15,10))
                        figureindex = figureindex + 1                        
                        plt.plot(senyal)
                        for xc in marques:
                            plt.axvline(x=xc, color='g', linestyle='--')

                        #plt.plot(peakpos2, peakval2, 'rD')
                        
                        clear = lambda: os.system('cls')
                        clear()


                #Removing the unused values of the meanBPM array
                meanBPM = meanBPM[:int(iteration)]
                #Storing the meanBPM of this channel at channelBPM parameter
                channelBPM.append(meanBPM)
                
                if (plotting == 0): 
                    #0=seconds | 1=minutes | 2=hours
                    timescale = 0 
                    timeaxis = np.linspace(0,(len(meanBPM)*(timeframe/(60**timescale))),len(meanBPM))               
                    figure = plt.figure(figureindex,figsize=(20,10))
                    figureindex = figureindex + 1
                    plt.ylim([0,230])
                    plt.plot(timeaxis,meanBPM)
                    plt.plot(timeaxis,[float('nan') if x==0 else x for x in meanBPM*clipped[1:]])
                    if (timescale == 0):
                        plt.xlabel('Seconds')
                    elif (timescale==1):
                        plt.xlabel('Minutes')
                    elif (timescale==2):
                        plt.xlabel('Hours')
                    else:
                        plt.xlabel('Unknown')  
                    plt.ylabel('BPM')
                    
                    date = datetime.fromtimestamp(time()).strftime('%Y-%m-%d ')
                    if not os.path.exists(date+filename):
                        os.makedirs(date+filename)
                    figure.savefig(os.getcwd()+'/'+date+filename+'/channel'+str(channeliteration)+'.png')
                    logging.error('File '+str(filename)+', channel '+str(channeliteration)+' processed successfully.')
                    
                    #Plot all channels meanBPMs in one single plot.            
                    figure2 = plt.figure(figureindex,figsize=(20,10))
                    figureindex = figureindex + 1
                    plt.ylim([70,90])
                    
                    plt.ylabel('BPM')
                    if (timescale == 0):
                        plt.xlabel('Seconds')
                    elif (timescale==1):
                        plt.xlabel('Minutes')
                    elif (timescale==2):
                        plt.xlabel('Hours')
                    else:
                        plt.xlabel('Unknown') 
                        
                    for i in np.arange(len(channelBPM)):
                        plt.plot(timeaxis,channelBPM[i]) 
                    figure2.savefig(os.getcwd()+'/'+date+filename+'/allchannels.png')

        
#            except ValueError:
#                logging.error('An exception has occurred while processing file '+str(filename)+', channel '+str(channeliteration)+'.')
    
#xcoords = np.loadtxt(os.getcwd()+'/rrFile.txt')*fs
#for xc in xcoords:
#    plt.axvline(x=xc)
        

                    correctedBPM = np.arange(len(channelBPM[0]))
                    ### Corrected BPM from the 3 channels
                    for i in np.arange(len(channelBPM[0])):
                        number = len(channelBPM)-1
                        y = 1
                        deviation = 1000
                        if number == 0:
                            correctedBPM = channelBPM[number-1]
                        else:
                            while number >= 0:
                                while number-y >= 0:
                                    if np.std([channelBPM[number][i],channelBPM[number-1][i]])<deviation:
                                        deviation = np.std([channelBPM[number][i],channelBPM[number-1][i]])
                                        correctedBPM[i] = np.mean([channelBPM[number][i],channelBPM[number-1][i]])
                                    y = y+1
                                number = number-1
                                y = 1
                            
            
                    #Plot the correctedBPM over time.
                    figure3 = plt.figure(figureindex,figsize=(20,10))
                    figureindex = figureindex + 1
                    plt.ylim([0,230])
                    
                    plt.ylabel('BPM')
                    if (timescale == 0):
                        plt.xlabel('Seconds')
                    elif (timescale==1):
                        plt.xlabel('Minutes')
                    elif (timescale==2):
                        plt.xlabel('Hours')
                    else:
                        plt.xlabel('Unknown') 
                        
                    plt.plot(timeaxis,correctedBPM)
                    figure3.savefig(os.getcwd()+'/'+date+filename+'/correctedBPM.png')
        
                    
                    
                    ###  STATISTICS ###
                    BPMrest = 65   # to be defined by the user
                    BPMmax = 190   # to be defined by the user
                    
                    # Average Heart Rate Reserve
                    HRRavg = (np.mean(correctedBPM) - BPMrest) / (BPMmax - BPMrest) * 100
                    
                    # TRaining IMPulse
                    durationminutes = int(len(BPM)*timeframe) / 60 #* 60**timescale # last value from the timeaxis converted to minutes
                    genderfactor = 1.92 #men
                    #genderfactor = 1.64 #women
                    trainingimpulse = durationminutes * HRRavg * 0.64 ** (genderfactor * HRRavg)
                    TRIMPhour = trainingimpulse / durationminutes / 60
                    
                    # Percentiles
                    perc25 = np.percentile(correctedBPM,25)
                    perc50 = np.percentile(correctedBPM,50)
                    perc75 = np.percentile(correctedBPM,75)
                    
                    # Zones counting
                    #Z1 = ((100 <= correctedBPM) & (correctedBPM < 120)).sum()
                    #Z2 = ((120 <= correctedBPM) & (correctedBPM < 140)).sum()
                    Z3 = ((140 <= correctedBPM) & (correctedBPM < 150)).sum()
                    Z4 = ((150 <= correctedBPM) & (correctedBPM < 160)).sum()
                    Z5 = ((160 <= correctedBPM) & (correctedBPM < 170)).sum()
                    Z6 = ((170 <= correctedBPM) & (correctedBPM < 180)).sum()
                    Z7 = ((180 <= correctedBPM) & (correctedBPM < 185)).sum()
                    Z8 = ((185 <= correctedBPM) & (correctedBPM < 190)).sum()
                    Z9 = ((190 <= correctedBPM) & (correctedBPM < 195)).sum()
                    #Z10 = ((195 <= correctedBPM) & (correctedBPM < 210)).sum()  
                    
                    Zs = np.array([Z3, Z4, Z5, Z6, Z7, Z8, Z9])
                    Ztotal = Z3+Z4+Z5+Z6+Z7+Z8+Z9
                    Zsperc = Zs / Ztotal * 100
                    
                    # Zones plotting
                    figure4 = plt.figure(figureindex,figsize=(20,10))
                    figureindex = figureindex + 1
                    #plt.ylim([0,100])
                    plt.ylabel('%')
                    
                    Zsperc_labels = ['Z1 \n 140-150','Z2 \n 150-160','Z3 \n 160-170','Z4 \n 170-180','Z5 \n 180-185','Z6 \n 185-190','Z7 \n 190-195','Z8 \n 195-210']
                    plt.bar(range(len(Zsperc)),Zsperc, align='center')
                    plt.xticks(range(len(Zsperc)),Zsperc_labels,size='small')
                    figure4.savefig(os.getcwd()+'/'+date+filename+'/HRzones.png')
                    
                    DATA = DATA, filename+' - TRIMP: '+str(np.round(trainingimpulse, decimals=2))+', TRIMPhour: '+str(np.round(TRIMPhour, decimals=2))+' Percentile 25, 50, 75: '+str(np.round(perc25, decimals=2))+', '+str(np.round(perc50, decimals=2))+', '+str(np.round(perc75, decimals=2))
        
        
        
                    #Temporal indexes
                    #Mean RR
                    correctedRR = (60 / correctedBPM) * 1000 #BPM/60 (beats per second) *1000(to ms)
                    
                    figure5 = plt.figure(figureindex,figsize=(20,10))
                    figureindex = figureindex + 1
                    #plt.ylim([0,230])
                    
                    plt.ylabel('RR')
                    if (timescale == 0):
                        plt.xlabel('Seconds')
                    elif (timescale==1):
                        plt.xlabel('Minutes')
                    elif (timescale==2):
                        plt.xlabel('Hours')
                    else:
                        plt.xlabel('Unknown') 
                        
                    plt.plot(timeaxis,correctedRR)
                    figure5.savefig(os.getcwd()+'/'+date+filename+'/correctedRR.png')
                    
                    meanRR = np.mean(correctedRR)
                    
                    
                    #SDNN
                    SDNN = np.sqrt(np.sum((correctedRR-np.mean(correctedRR))**2)/(len(correctedRR)-1))
                    SDNNw = (correctedRR-np.mean(correctedRR))**2
                    
#                    figure6 = plt.figure(figureindex,figsize=(20,10))
#                    figureindex = figureindex + 1
#                    plt.ylim([0,3*np.mean(SDNN)])
#                    
#                    plt.ylabel('SDNN')
#                    if (timescale == 0):
#                        plt.xlabel('Seconds')
#                    elif (timescale==1):
#                        plt.xlabel('Minutes')
#                    elif (timescale==2):
#                        plt.xlabel('Hours')
#                    else:
#                        plt.xlabel('Unknown') 
#                        
#                    plt.plot(timeaxis,SDNN)
#                    figure6.savefig(os.getcwd()+'/'+date+filename+'/SDNN.png')
                    
                    
                    #RMSSD
                    RMSSD = np.sqrt(np.sum((correctedRR[1:]-correctedRR[:-1])**2)/(len(correctedRR)-1))
                    RMSSDw = (correctedRR[1:]-correctedRR[:-1])**2
                    
#                    figure7 = plt.figure(figureindex,figsize=(20,10))
#                    figureindex = figureindex + 1
#                    plt.ylim([0,20])
#                    
#                    plt.ylabel('RMSSD')
#                    if (timescale == 0):
#                        plt.xlabel('Seconds')
#                    elif (timescale==1):
#                        plt.xlabel('Minutes')
#                    elif (timescale==2):
#                        plt.xlabel('Hours')
#                    else:
#                        plt.xlabel('Unknown') 
#                        
#                    plt.plot(timeaxis[:-1],RMSSD)
#                    figure7.savefig(os.getcwd()+'/'+date+filename+'/RMSSD.png')                    
                    
                    
                    #SDSD
                    SDSD = np.sqrt(np.sum((correctedRR[1:]-correctedRR[:-1]-RMSSD**2)**2)/(len(correctedRR)-2))
                    SDSDw = (correctedRR[1:]-correctedRR[:-1]-RMSSD**2)**2
                    
#                    figure8 = plt.figure(figureindex,figsize=(20,10))
#                    figureindex = figureindex + 1
#                    plt.ylim([0,50])
#                    
#                    plt.ylabel('SDSD')
#                    if (timescale == 0):
#                        plt.xlabel('Seconds')
#                    elif (timescale==1):
#                        plt.xlabel('Minutes')
#                    elif (timescale==2):
#                        plt.xlabel('Hours')
#                    else:
#                        plt.xlabel('Unknown') 
#                        
#                    plt.plot(timeaxis[:-1],SDSD)
#                    figure8.savefig(os.getcwd()+'/'+date+filename+'/SDSD.png')                    
                                        
                    
                    #Export to csv
                    statistics = [timeaxis, correctedBPM, correctedRR, SDNNw, RMSSDw, SDSDw]
                    csvfile = os.getcwd()+'/'+date+filename+'/statistics.csv'
                    with open(csvfile, "w") as output:
                        writer = csv.writer(output, lineterminator='\n')
                        writer.writerows(statistics)

                    
                    
        clear = lambda: os.system('cls')
        clear()
        
