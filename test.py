#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 12:56:09 2017

@author: oriolrd
"""

from tkinter import filedialog
from tkinter import *
 
def openFile():
    root.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
    return root.filename

def saveFile():
    root.filename =  filedialog.asksaveasfilename(initialdir = "/",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
    return root.filename

class MyFirstGUI:
    def __init__(self, master):
        if filename is 0:
            root.filename = "No file selected"
            
        self.master = Tk()
        master.title("Basic ECG GUI")

        self.label = Label(master, text="ECG")
        self.label.pack()
        
        self.selectFile_button = Button(master, text="Select File", command=self.selectFile)
        self.selectFile_button.pack()
        
        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack()
        
        T = Text(root, height=2, width=30)
        T.pack()
        T.insert(END, "Current File:\n"+root.filename+"\n")

        mainloop()
        
    def selectFile(self):
        root.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("all files","*.*"),("jpeg files","*.jpg")))
        filename = 1
        self.depositLabel['text'] = 'change the value'
        return root.filename, filename
    
root = Tk()
filename = 0
my_gui = MyFirstGUI(root)
root.mainloop()