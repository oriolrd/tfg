#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  9 02:03:17 2018

@author: oriolrd
"""
import numpy as np
import matplotlib.pyplot as plt


derSenyal = np.diff(senyal)
numMostres = len(derSenyal)
offset = 50
llindar = .5 * max(derSenyal[1:2500])
comptador = 0
mostra = 0
marques = []

# BUCLE PRINCIPAL DE CERCA
continuar = True
while continuar:
    if derSenyal[mostra] > llindar:
        buscar = True
        while buscar:
            if derSenyal[mostra] < 0:
                comptador = comptador + 1
                marques = np.append(marques,mostra)
                buscar = False
                llindar = np.mean([llindar, .5 * max(derSenyal[mostra + offset:min([mostra + (2500), numMostres])])])
                mostra = mostra + offset
                print(mostra)
                if mostra >= numMostres:
                    continuar = False
                    
            else:
                mostra = mostra + 1
                if mostra >= numMostres:
                    continuar = False
                    
    else:
        mostra = mostra + 1
        if mostra >= numMostres:
            continuar = False
            
            
## DIBUIXEM ELS RESULTATS
plt.plot(senyal)
for xc in marques:
    plt.axvline(x=xc, color='g', linestyle='--')
    
#t = (mslice[1 / 250:1 / 250:numMostres / 250]).cT
#figure(mstring('box'), mstring('on'), mstring('hold'), mstring('on'))
#plot(t, senyal(mslice[1:numMostres]))
#plot(t, derSenyal, mstring('r'))
#axis(mstring('tight'))
#plot(mcat([marques, marques]).cT / 250, repmat(get(gca, mstring('Ylim')).cT, size(marques.cT)), mstring('k--'))
#title(mstring('ECG'))
#xlabel(mstring('temps (s)'))
#hold(mstring('off'))
#xlim(mcat([2, 4]))
#pan(mstring('xon'))