function marques = detecrc(senyal)

% DEFINICIONS
derSenyal  = diff(senyal);
numMostres = length(derSenyal);
offset     = 50;
llindar    = .5*max(derSenyal(1:2500));
comptador  = 0;
mostra     = 400;
                                         
% BUCLE PRINCIPAL DE CERCA
continuar = true;
while continuar
  if derSenyal(mostra) > llindar
    buscar = true;
    while buscar
      if derSenyal(mostra) < 0
        comptador = comptador + 1;
        marques(comptador, 1) = mostra;
        buscar = false;
        llindar = mean([llindar .5*max(derSenyal(mostra+offset:min([mostra+(2500) numMostres])))]);
        mostra = mostra + offset;
        if mostra >= numMostres
          continuar = false;
        end
      else
        mostra = mostra + 1;
        if mostra >= numMostres
          continuar = false;
        end
      end
    end
  else
    mostra = mostra + 1;
    if mostra >= numMostres
      continuar = false;
    end
  end
end

% DIBUIXEM ELS RESULTATS
t = (1/250:1/250:numMostres/250)';
figure; box on; hold on;
plot(t, senyal(1:numMostres));
plot(t, derSenyal, 'r');
axis tight;
plot([marques marques]'/250, repmat(get(gca, 'Ylim')', size(marques')), 'k--');
title('ECG')
xlabel('temps (s)')
hold off;
xlim([2 4]);
pan xon;